;;;;;;;      Wave Tracking Script
;;;;;;;      Create OW ; this takes a while should save 2d field to file
;;;;;;;      Find all maxima and save to array
;;;;;;;      Identify start points within specified region
;;;;;;;      Loop through start points find neighbours
;;;;;;;      iterate until convergence
;;;;;;;      mark all points as used.  remove from pool of start points and way points

undef("phase_speed")
function phase_speed(track)
begin
ps = center_finite_diff(track, 1, False, 0)
return(ps)
end


undef("loc_max")
function loc_max(x, cyclic, thres)
local maxs, new_max, max_thres
begin
  maxs = local_max(x, cyclic, 0.)
  if(all(maxs.eq.0))
     return(0)
  end if
  max_thres = ind(maxs@maxval .ge. thres )
  dimnames = getVarDimNames(x)
  if(all(ismissing(max_thres)))
      new_max = 0.
  else
    new_max = dimsizes(max_thres)
    new_max@maxval = maxs@maxval(max_thres)
    new_max@$dimnames(1)$ = x&$dimnames(1)$(maxs@xi(max_thres))
    new_max@$dimnames(0)$ = x&$dimnames(0)$(maxs@yi(max_thres))
  end if
  return(new_max)
end

undef("loc_max_1d")
function loc_max_1d(x, cyclic, thres)
local maxs, new_max, max_thres
begin
  maxs = local_max_1d(x, cyclic, 0.,1)
  max_thres = ind(x(maxs) .gt. thres )
  maxs@maxval = x(maxs(max_thres))
  return(maxs(max_thres))
end


undef("unique_2")
function unique_2(x,n[2])
local x1, x2, nm, x, i, dims
begin
  dims = dimsizes(x)
  x1 = x
  x2 = new( dims, typeof(x1))

  n0 = n(0)
  n1 = n(1)

  do i=0, dims(1) -1
    if(.not.any(x2(n0,:) .eq. x1(n0,i) ) )
            x2(:,i) = x1(:,i)

    else
       xind := ind(x2(n0,:).eq. x1(n0,i) )
       if( .not.any( x2(n1,xind) .eq. x1(n1,i) ) )
          x2(:,i) = x1(:,i)
       end if
    end if
  end do

  nm = ind(.not.ismissing(x2(n0,:)) )
  if(all(ismissing(nm)))
    print("Fail; Whole Array is missing for some reason")
    return(False)
  end if

  return(x2(:,nm))
end



undef("filter_locations")
function filter_locations(locs, radius)
local lats, lons, maxs, radius, i, nlats, nlons, nmaxs, dist, distind
begin
lats = locs(1,:)
lons = locs(0,:)
maxs = locs(2,:)

nlats = lats
nlons = lons
nmaxs = maxs

do i=0, dimsizes(lats)-1
    dist := gc_latlon(lats(i), lons(i), lats, lons, 0, 4)
    distind := ind(dist.le. radius .and. maxs .ge. (maxs(i)*0.5) )
    nlats(i) = dim_avg_wgt_Wrap(lats(distind), maxs(distind)^2,1 )
    nlons(i) = dim_avg_wgt_Wrap(lons(distind), maxs(distind)^2,1 )
    nmaxs(i) = dim_avg_wgt_Wrap(maxs(distind), maxs(distind)^2,1 )
end do
retlocs = unique_2( (/nlons, nlats, nmaxs/), (/0,1/) )
;retlocs = (/nlons, nlats, nmaxs/)
return( retlocs )
end

 undef("filtered_maxs")
function filtered_maxs(field, radius, cyclic)
local locmin, x, y, z, locs, dims, hdims, ct
begin
locmin = loc_max(field, cyclic, 0.2)
x=locmin@lon; field&lon(locmin@xi)                ; get lat/lon points of minima
y=locmin@lat; field&lat(locmin@yi)
z = locmin@maxval
locs = (/x,y,z/)
dims = dimsizes(locs)
hdims =(/0,0/)
ct = 0
do while( dims(1).ne.hdims(1))
    hdims = dimsizes(locs)
    locs := filter_locations(locs, radius)
    dims = dimsizes(locs)
    ct = ct+1
    if(ct.gt.25)
        print("breaking")
       break
    end if
end do
return(locs)
end



undef("hov_max")
function hov_max(x, cyclic, thresh)
local maxs, new_max, max_thres
begin
do t=0, dimsizes(x&time)-1
  imax := loc_max_1d(x(t,:), cyclic, thresh)
  time := new(dimsizes(imax),typeof(x&time))
  time = x&time(t)
  if(isvar("imaxs"))
  imaxs := array_append_record(imaxs, imax,0)
  maxval := array_append_record(maxval, imax@maxval,0)
  times := array_append_record(times, time,0)
  else
  imaxs = imax
  maxval = imax@maxval
  times = time
  end if
end do

locs = new( (/2, dimsizes(imaxs)/), float)
locs(0,:) =tofloat(times)
locs(1,:) = x&lon(imaxs)
locs@val = imaxs
locs@maxval = maxval
return(locs)
end

undef("dot2dot_astar")
procedure dot2dot_astar(x, i, track, dir, dt, zes )
local x\        ; (time | lon ) x n
      ,dt\      ; time difference in file
      ,ps\      ; current phase speeds of track
      ,dir\     ; positive or negative to flip track direction
      ,ti\      ; indices holding maxima from next time
      ,xld\     ; longitude difference from current maxima
      ,li\      ; indices for lons within range
      ,mi\      ; sub-indice for max if multiple lons in range.
      ,combined_d\
      , cur_ps\
      , ps_std
begin
; run through every point, search downstream for another point.
;print(num( (abs(xld).lt.3.) .and. (abs(xtd).lt.6)))
;ps = -2.0
;ps_limit = 4.0; degrees / 6 hours ~ 15m/s


  if(.not.isatt(track, "track_length"))
   track@track_length = num(.not.imissing(track))
  end if
  if( track@track_length .gt.3)
      ps = phase_speed(track)
      curr_ps = avg(ps) < -1.2 > -3.5
      ps_std = stddev(ps) < 1.5 > 4.
  else
      curr_ps = -1.8   ;;  roughly 7ms   set for AEW.
      ps_std = 4.
  end if
  curr_ps = curr_ps * dir

  ;;; uses phase speed to find points with 4 degrees of expected location at next time period
  ti := ind( (x(0,:)-x(0,i)) .eq. dt*dir .and. abs( (x(1,:) - x(1, i)) - 1.0*curr_ps ) .le. ps_std )   ;
  print(ti)
  if(all(ismissing(ti)))
    return
  end if
  if(dimsizes(ti).eq.1) then
    x@logic(ti) = False
    track( {x(0,ti)} ) =  (/ x(1,ti) /)
    track@track_length = track@track_length + 1
    track@i = i
    track@avg_val = track@avg_val + x@maxval(ti)
    dot2dot_astar(x, ti, track, dir, dt, zes )
  else
    return
  end if
  zes(:, ti) =  x(:,ti)

  do tii= 0, dimsizes(ti)-1
    dot2dot_astar(x, ti(tii), track, dir, dt ,zes)
  end do
  ;; calculate change in location and change in value
;  if(dimsizes(ti) .gt. 1)
;      xld :=  (x(1,ti) - x(1, i) ) - curr_ps    ; calc phase speed diff from current
;      vald :=  x@maxval(ti) - x@maxval(i)  ; calc change in value from current
;      combined_d := ( (xld / curr_ps )*0.5 )+ ( ( vald / x@maxval(i) ) *0.5)
;      tt = ti( minind( abs(combined_d - 1 ) ))  ;  1 would be the same vort and exact phase_speed
;  else
;    tt = ti(0)
;  end if
;tt= ti

;  x@logic(tt) = False
;  track( {x(0,tt)} ) =  (/ x(1,tt) /)
;  track@track_length = track@track_length + 1
;  track@i = i
;  track@avg_val = track@avg_val + x@maxval(tt)
 ; dot2dot_astar(x, tt, track, dir, dt ,zes)

end





undef("dot2dot")
procedure dot2dot(x, i, track, dir, dt )
local x\        ; (time | lon ) x n
      ,dt\      ; time difference in file
      ,ps\      ; current phase speeds of track
      ,dir\     ; positive or negative to flip track direction
      ,ti\      ; indices holding maxima from next time
      ,xld\     ; longitude difference from current maxima
      ,li\      ; indices for lons within range
      ,mi\      ; sub-indice for max if multiple lons in range.
      ,combined_d\
      , cur_ps\
      , ps_std
begin
; run through every point, search downstream for another point.
;print(num( (abs(xld).lt.3.) .and. (abs(xtd).lt.6)))
;ps = -2.0
;ps_limit = 4.0; degrees / 6 hours ~ 15m/s
  if(.not.isatt(track, "track_length"))
   track@track_length = num(.not.imissing(track))
  end if
  if( track@track_length .gt.3)
      ps = phase_speed(track)
      curr_ps = avg(ps) < -1.2 > -3.5
      ps_std = stddev(ps) < 1.5 > 4.
  else
      curr_ps = -1.8   ;;  roughly 7ms   set for AEW.
      ps_std = 4.
  end if
  curr_ps = curr_ps * dir

  ;;; uses phase speed to find points with 4 degrees of expected location at next time period
  ti := ind( (x(0,:)-x(0,i)) .eq. dt*dir .and. abs( (x(1,:) - x(1, i)) - 1.0*curr_ps ) .le. ps_std )   ;
  if(all(ismissing(ti)))
    return
  end if

  ;; calculate change in location and change in value
  if(dimsizes(ti) .gt. 1)
      xld :=  (x(1,ti) - x(1, i) ) - curr_ps    ; calc phase speed diff from current
      vald :=  x@maxval(ti) - x@maxval(i)  ; calc change in value from current
      combined_d := ( (xld / curr_ps )*0.7 )+ ( ( vald / x@maxval(i) ) *0.3)
      tt = ti( minind( abs(combined_d - 1 ) ))  ;  1 would be the same vort and exact phase_speed
  else
    tt = ti(0)
  end if
;tt= ti

  x@logic(tt) = False
  track( {x(0,tt)} ) =  (/ x(1,tt) /)
  track@track_length = track@track_length + 1
  track@i = i
  track@avg_val = track@avg_val + x@maxval(tt)
  dot2dot(x, tt, track, dir, dt )
end


undef("check_overlap")
function check_overlap(tracks, track, tr)
local tt
begin
if( (track@avg_val / track@track_length) .lt. 0.05 )
;print( (track@avg_val / track@track_length) )
return(False)
end if
do tt=0, tr-1
    if(num(track .eq. tracks(tt,:)) .gt. 8 )  ;  if tracks overlap
      tdist = (min(track) -max(track))
      sdist = (min(tracks(tt,:)) -max(tracks(tt,:)) )
        if( tdist .le. 1.1*sdist )   ;  if newtrack is 10% longer take that one
   ;       print(tdist +" "+ sdist )
          tracks(tt,:) = (/ track /)
          return(False)
        end if
        if( tdist .le. 0.8*sdist )      ; if newtrack is within 80% of old one, check which is most westward
          tps := phase_speed(track)
          sps := phase_speed(tracks(tt,:))
          if( num(tps.gt.0) .lt. num(sps.gt.0) )
;print(num(tps.gt.0) +" "+ num(sps.gt.0) )
              tracks(tt,:) = (/ track /)
              return(False)
          end if
        end if
        return(False)
    end if
end do
return(True)
end


undef("check_overlap_2d")
function check_overlap_2d(tracks)
local tt,ttt, track_dims, overlap, ovlp_m8, ovlp_p8,tt_std, ttt_std, ret_ind, nmi
begin
track_dims := dimsizes(tracks)

do tt=0, track_dims(1)-1
do ttt=0, track_dims(1)-1
if(tt.eq.ttt)then continue end if
  if(num( sqrt( (tracks(1, tt, :)-tracks( 1, ttt, :))^2 + (tracks(0, tt, :)-tracks( 0, ttt, :))^2).le.4).gt.6)
    print(tt +"  "+ ttt)
    overlap := ind(sqrt( (tracks(1, tt, :)-tracks( 1, ttt, :))^2 + (tracks(0, tt, :)-tracks( 0, ttt, :))^2).le.4)

;;;;   if track tt doesn't last 2 days without ttt, while ttt does last 2 days. Kill it!!!
    if( num(.not.ismissing(tracks(1,tt,:))) - dimsizes(overlap)  .lt. 8 \
    .and. num(.not.ismissing(tracks(1,ttt,:))) - dimsizes(overlap)  .gt. 8  )
          tracks(:,tt,:) = tracks@_FillValue
          continue
          print("tt killed")
    end if

;;;;  vice versa ttt without tt kill it!!!
    if( num(.not.ismissing(tracks(1,ttt,:))) - dimsizes(overlap)  .lt. 8 \
    .and. num(.not.ismissing(tracks(1,tt,:))) - dimsizes(overlap)  .gt. 8  )
          tracks(:,ttt,:) = tracks@_FillValue
          continue
          print("ttt killed")
    end if


;;;;  keep track that has lowest phase speed std dev over 4 days of overlap start
    ovlp_m8 = min(overlap)-8 > 0
    ovlp_p8 = min(overlap)+8 < track_dims(2)-1
    tt_std  = stddev( phase_speed(tracks(0,tt,ovlp_m8:ovlp_p8)))+stddev( phase_speed(tracks(1,tt,ovlp_m8:ovlp_p8)))
    ttt_std = stddev( phase_speed(tracks(0,ttt,ovlp_m8:ovlp_p8)))+stddev( phase_speed(tracks(1,ttt,ovlp_m8:ovlp_p8)))

    if(tt_std .lt. ttt_std)then     kill = ttt    else      kill= tt   end if
    tracks(:, kill, min(overlap):max(overlap))  = tracks@_FillValue

;;;; keep which ever end of the track was longer.
    if( num(.not.ismissing(tracks(1,kill, max(overlap):))) .gt. num(.not.ismissing(tracks(1,kill, :min(overlap)))) )
        tracks(:, kill, :min(overlap))  = tracks@_FillValue
    else
        tracks(:, kill, max(overlap):)  = tracks@_FillValue
    end if
  end if
end do
end do

ret_ind = new( track_dims(1), logical)
ret_ind = True
do tt=0, track_dims(1)-1
if(all(ismissing(tracks(1,tt,:))))
ret_ind(tt) = False
end if
end do

nmi = ind(ret_ind .eq. True)

return(tracks(:,nmi,:))
end


undef("filter_tracks")
function filter_tracks(tracks, lon_bds, ntime, nlon)
begin

logic = new( dimsizes(tracks(:,0)), logical)
logic = True
do tr=0, dimsizes(tracks(:,0))-1
    if( (num( .not. ismissing(tracks(tr,:))) .lt. ntime) \;; cull 1 if they don't last 2 days; boot them
        .or. (abs( max(tracks(tr,:)) - min(tracks(tr,:)) ) .lt. nlon )\  ; ; cull 2; if they dont move 10 degrees of lon
        .or. (.not.any(tracks(tr,:).gt.lon_bds(0) .and. tracks(tr,:) .lt. lon_bds(1))) );; cull 3; if they are not within lon bounds
          logic(tr) = False
    end if
end do

do tr=0,dimsizes(tracks(:,0))-1
    if(logic(tr))
       do t=0,dimsizes(tracks(:,0))-1
       if(t.eq.tr)
         continue
       end if
       if( any(tracks(tr,:) .eq. tracks(t,:)) )
         tr_opts = (/tr, t/)
         tr_dims = dim_num_n( .not.ismissing(tracks(tr_opts, :)),1)
         if( tr_dims(0).eq.tr_dims(1) )
            tr_lens = abs( (/ min(tracks(tr_opts(0),:)) - max(tracks(tr_opts(0),:)), min(tracks(tr_opts(1),:)) - max(tracks(tr_opts(1),:)) /) )
            ts = minind(tr_lens)
        else
            ts = minind(tr_dims)
        end if
            logic(tr_opts(ts)) = False
       end if
     end do
   end if
end do



return( tracks(ind(logic.eq. True), :))
end



undef("filter_tracks2")
function filter_tracks2(tracks)
begin

ntime = 8
nlon = 10
lon_bds = (/-45, 0/)
return( filter_tracks(tracks, lon_bds, ntime, nlon) )

end




undef("refine_locations")
function refine_locations(track, field)
local refined_lon\
      , refined_lat\
      , t\
      , maxima\
      , dist\
      , maxi\
      , disti\
      , newtrack
begin

refined_lat = track(0,:)
refined_lon = track(1,:)

missing_ct = 0
last_max = 0.

do t=0, dimsizes(refined_lon)-1
    if(missing_ct .ge. 4)     ;;;;   if the last 3 locations have been weak, then give up.
        refined_lat(t-missing_ct:) = refined_lat@_FillValue
        refined_lon(t-missing_ct:) = refined_lon@_FillValue
        break
    end if

     if(ismissing(refined_lon(t)) ) ;; if this time isn't defined then lets guess and work from there.
         if( (t.ge.1) .and. (.not.ismissing(refined_lon(t-1))) .and. (last_max .gt. 0.25) )
             refined_lon(t) = refined_lon(t-1) - 1.5
         else
             continue
         end if
   end if

    ;;; previous checks should prevent code from getting here unless defined.

    refined_lat(t) = where(ismissing(refined_lat( (t-1)>0 )), 8., refined_lat( (t-1)>0 ))

    if(refined_lon(t) .gt. -20  )
        refined_lat(t) =  refined_lat(t) > 5 < 15
    end if


   maxima := loc_max(field(t,{refined_lat(t)-4:refined_lat(t)+4},{refined_lon(t)-4:refined_lon(t)+4}), False, 0.)
    if(maxima.eq.0)
        missing_ct = missing_ct+1
        last_max = 0.
     continue
   end if

   dist := gc_latlon(refined_lat(t), refined_lon(t), maxima@lat, maxima@lon, 2, 4)

   maxi := ind(maxima@maxval .ge. 0.5*max(maxima@maxval) .and. dist.le. 300) ; max within 50% and 300km repeated below as well
    if(all(ismissing(maxi)))
     maxi := ind(maxima@maxval .ge. 0.5*max(maxima@maxval) )
   end if
   refined_lon(t)=   dim_avg_wgt(maxima@lon(maxi), maxima@maxval(maxi), 0)
   refined_lat(t)=   dim_avg_wgt(maxima@lat(maxi), maxima@maxval(maxi), 0)
   last_max = dim_avg_wgt(maxima@maxval(maxi), maxima@maxval(maxi), 0)
end do


t=min(ind(.not.ismissing(refined_lon) ) )
if(ismissing(t))
    return(track)
end if
missing_ct = 0
last_max = 0.
do while( t .gt. 0)
t = t-1
    if(missing_ct .ge. 4)     ;;;;   if the last 3 locations have been weak, then give up.
        refined_lat(:t+missing_ct) = refined_lat@_FillValue
        refined_lon(:t+missing_ct) = refined_lon@_FillValue
        break
    end if

     if(ismissing(refined_lon(t)) ) ;; if this time isn't defined then lets guess and work from there.
         if( (t.ge.0) .and. (.not.ismissing(refined_lon(t+1))) .and. (last_max .gt. 0.25) )
             refined_lon(t) = refined_lon(t+1) - (1.5 *-1)
         else
             continue
         end if
   end if

    ;;; previous checks should prevent code from getting here unless defined.
   refined_lat(t) = where(ismissing(refined_lat( (t+1)>0 )), 8., refined_lat( (t+1)>0 ))

    if(refined_lon(t) .gt. -20 )
        refined_lat(t) =  refined_lat(t) > 5 < 15
    end if


   maxima := loc_max(field(t,{refined_lat(t)-4:refined_lat(t)+4},{refined_lon(t)-4:refined_lon(t)+4}), False, 0.)
    if(maxima.eq.0)
        missing_ct = missing_ct+1
        last_max = 0.
     continue
   end if

   dist := gc_latlon(refined_lat(t), refined_lon(t), maxima@lat, maxima@lon, 2, 4)

   maxi := ind(maxima@maxval .ge. 0.5*max(maxima@maxval) .and. dist.le. 300) ; look for max with 50% and within 300km
    if(all(ismissing(maxi)))
     maxi := ind(maxima@maxval .ge. 0.5*max(maxima@maxval) )
   end if
   refined_lon(t)=   dim_avg_wgt(maxima@lon(maxi), maxima@maxval(maxi), 0)
   refined_lat(t)=   dim_avg_wgt(maxima@lat(maxi), maxima@maxval(maxi), 0)
   last_max = dim_avg_wgt(maxima@maxval(maxi), maxima@maxval(maxi), 0)
end do




track(0,:) = refined_lat
track(1,:) = refined_lon

track(0,:) = where(ismissing(track(1,:)), track@_FillValue, track(0,:) )
track(1,:) = where(ismissing(track(0,:)), track@_FillValue, track(1,:) )
return(track)
end



undef("track_hov")
function track_hov(hov)
;; 1. find locals maxs per time
;; 2. loop through all open points working along the track
;;      close points when passed through, start again at any open points.
;;      closed points are not excluded from future tracks, only starting new ones.
;;      apply thresholds as each track is created and cull if they don't meet.
local maxs\     ; one d list of maxima
    , logic\    ; one d list of open/closed points
    , tracks\   ; return array of (tracks x time)
    , tr\       ; track iterator
    , xo\       ; index of starting point for tracks
    , i\        ; time iterator
    , li\        ; index of next maxima
    , ftracks
begin

maxs := hov_max(hov(:,:), False, 0.)    ;  find local maxima per time greater than 0.
maxs@logic =  conform_dims(dimsizes(maxs(0,:)),True,  -1) ;; will initialise array of True

tracks := new( (/ dimsizes(maxs(0,:)), dimsizes(hov&time)/), float)
tracks!1 = "time"
tracks&time = hov&time
tracks@avg_val = new( dimsizes(maxs(0,:)) , float)

tr = 0
do while( any(maxs@logic) )
  i= min(ind(maxs@logic))
;  print( (/ num(maxs@logic)/) )
  track = tracks(tr, :)
  track@track_length = 1
  track@avg_val := maxs@maxval(i)
  track@i = i
  maxs@logic(i) = False            ; close point as we move over it.
  track({maxs(0,i)} ) =  (/ maxs(1,i) /)
  dot2dot(maxs, i, track(:), -1, 6 )            ;;; search backward
  dot2dot(maxs, track@i, track(:), 1, 6 )       ;;; search forward

  if(track@track_length .gt. 6)
   if( check_overlap(tracks, track, tr) )
      tracks(tr,:) = (/ track /)
      tracks@avg_val(tr) = (/ track@avg_val /)
      tr = tr +1
    end if
  end if
end do

ftracks = filter_tracks(tracks, (/-90,30/), 6, 5 )
return(ftracks)
end


;alltracks := track_hov(ow2d(:,{-120:45}) )
;track_lons = filter_tracks(alltracks)

;track_lats = track_lons
;track_lats = 10.

;tracks3d = (/track_lats, track_lons /)
;tracks3d!2 = "time"
;tracks3d&time = track_lons&time

;do tr=0, dimsizes(tracks3d(0,:,0))-1
;tracks3d(:,tr,:) = (/ refine_locations(tracks3d(:,tr,:), ow_a) /)
;tracks3d(:,tr,:) = (/ refine_locations(tracks3d(:,tr,:), ow_a) /)
;end do

