c   NCLFORTSTART
      subroutine shear_vort(uwnd, vwnd, dxarr, dyarr, nx, ny, shvort)
      integer nx, ny
      real uwnd(nx,ny), vwnd(nx,ny)
      real dxarr(nx,ny), dyarr(nx,ny), shvort(nx,ny)
      integer  i, j, i1, i2, j1, j2, di, dj
      real vlen, wvec(2), v1, v2, u1, u2, ang
c   NCLEND
      shvort(:,:) = 0
      
      do i = 1, nx  
      do j = 1, ny
      
       i1 = max(i-1,1)  ;  i2 = min(i+1,nx)
       j1 = max(j-1,1)  ;  j2 = min(j+1,ny)
      
       if ( uwnd(i,j) /= missing_r8 .and. vwnd(i,j) /= missing_r8 ) then
      
         di = 0  ;  dj = 0
         vlen = sqrt(uwnd(i,j) * uwnd(i,j) + vwnd(i,j) * vwnd(i,j))
         wvec(1) = uwnd(i,j) / vlen  ;  wvec(2) = vwnd(i,j) / vlen
         ang = atan(vwnd(i,j)/uwnd(i,j))
      
         if ( uwnd(i1,j) /= missing_r8 ) then
           v1 = (wvec(1) * uwnd(i1,j) + wvec(2) * vwnd(i1,j)) * wvec(2)
           if ( i1 /= i )  di = di + 1
         else
           v1 = (wvec(1) * uwnd(i,j) + wvec(2) * vwnd(i,j)) * wvec(2)
         end if
      
         if ( uwnd(i2,j) /= missing_r8 ) then
           v2 = (wvec(1) * uwnd(i2,j) + wvec(2) * vwnd(i2,j)) * wvec(2)
           if ( i2 /= i ) di = di + 1
         else
           v2 = (wvec(1) * uwnd(i,j) + wvec(2) * vwnd(i,j)) * wvec(2)
         end if
      
         if ( uwnd(i,j1) /= missing_r8 ) then
           u1 = (wvec(1) * uwnd(i,j1) + wvec(2) * vwnd(i,j1)) * wvec(1)
           if ( j1 /= j )  dj = dj + 1
         else
           u1 = (wvec(1) * uwnd(i,j) + wvec(2) * vwnd(i,j)) * wvec(1)
         end if
      
         if ( uwnd(i,j2) /= missing_r8 ) then
           u2 = (wvec(1) * uwnd(i,j2) + wvec(2) * vwnd(i,j2)) * wvec(1)
           if ( j2 /= j )  dj = dj + 1
         else
           u2 = (wvec(1) * uwnd(i,j) + wvec(2) * vwnd(i,j)) * wvec(1)
         end if
      
         if ( di > 0 .and. dj > 0 ) then
         shvort(i,j)=(v2 - v1) / (real(di) * dxarr(i,j)) 
     &    -((u2 - u1)/(real(dj) * dyarr(i,j)) )                
         end if
      
       end if
      
      end do  
      end do
      
      return
      end