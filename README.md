# Wave Tracking though Hovmoller Analysis #

Updated and improved code to track maxima across hovmoller plots.  

Step 1 of tracking used by Brammer & Thorncroft 2015; http://journals.ametsoc.org/doi/abs/10.1175/MWR-D-15-0106.1


### How do I get set up? ###

* Written as NCL function, pass 2d array of data (time | lon) along with some basic settings
* 


### Who do I talk to? ###

* abrammer _at_ albany.edu