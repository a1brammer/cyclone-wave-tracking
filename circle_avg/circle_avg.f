
C       circle_avg_m
C           - Accounted for Latitide and loops over cyclic
C           - Expects cyclic data, without overlap point
C           - Uses haversine to calc distances, some error but less than a grid space
C           - Last edited 2013/03/22  A Brammer.
C   NCLFORTSTART
        subroutine circle_avg_m(indata, bkgrd, inlat, nx,ny,dxm)
        real indata(nx,ny), bkgrd(nx,ny), tempv, R
        real lat(ny), inlat(ny), divider, lat1, lat2, eps, fillvalue
        integer nx, ny,cx ,i,j,y, ii, iii,dx, dxi,dxmi
        integer rx, ry, tot_cx
        real dxm, clat(ny)
C NCLEND
         eps = 2.0E-07
         fillvalue = 9.96921e+36
         R=6371. ! Earth radius km.
         pi=(355./113.) ! pi Approx good to 6 decimal places

         dxmi = int(dxm) ! need an integer version of dx
c       cx defines the longitudinal grid points for each lat.

          dx  = int(  dxmi/(2*pi*(R/nx)) ) ! expects consistent spaced
          dxi = int(dx)  ! not sure why this is needed but indexing fails without it.

c      Convert latitude to radians and cos(lat)
         lat = inlat   ! work with temp variable so as to not overwrite.
         lat = lat*(pi/180.)   ! convert to radians
         clat = cos(lat)         ! Take cos of radians

        do i=1,nx  ! Loop across longitudes
            do j=(dx+2),(ny-dx-2)  ! loop lats excluding radius of circle
            tempv = 0.0
            y=0
            divider = 0.0
            do y=-dx,dx ! work way up circle
              lat1 = lat(j)  !  centre of circle
              lat2 = lat(j+y) ! vertical distance from cirlce centre.
              xr=acos(-((sin(lat1)*sin(lat2))
     +         -cos(dxmi/R))/(cos(lat1)*cos(lat2)))         !! haversine trig
              xd = int( xr/(pi/180.)/(360./nx) )
              cx = xd ! conv. to grid points.
                 do ii=(i-cx), (i+cx)  ! work across circle
                        iii = ii
                        if(iii.lt.1)then ! loop over break assumes global w/o cyclic
                            iii = iii+nx
                        end if
                        if(iii.gt.nx)then
                            iii = iii-nx
                        end if
                  if( abs(indata( iii , (j+y))-fillvalue).gt.eps )then    !! don't include missing values
                     tempv = tempv + (clat(j+y))*indata( iii , (j+y) )
                     divider = divider + (clat(j+y))
                  end if
C                 tempv = tempv + (clat(j+y))*indata( iii , (j+y) )
C                 divider = divider + (clat(j+y))
                    end do
             end do
             if(divider .lt. eps)then            !! if any non-missing calc average
                 bkgrd(i,j) = fillvalue
              else
                 bkgrd(i,j) =  tempv/divider
              end if
             tempv = 0.0      !  not sure why this is here, it's reset at the top of the loop
            enddo  ! end j loop
        enddo      ! end i loop
        bkgrd(:,1:(dxi+2)) = indata(:,1:(dxi+2))  ! Fill poles with unsmoothed data.
        bkgrd(:,(ny-dxi-2):ny) = indata(:,(ny-dxi-2):ny)
        return
        end

